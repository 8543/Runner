#include <UnitTest++/UnitTest++.h>
#include <thread>

#include "test.h"
#include <owWindow/Types/vector3.h>

using namespace OpenWorld;
int main() {
    auto ret = UnitTest::RunAllTests();

    Types::Instance root_ptr("Root");
    System::Window w("OpenWorld", "test.lua", &root_ptr, [&]() {

        for(const auto &i : root_ptr.c_getChildren()) {
            i->step();//
        }
    });

    return ret;
}