// Modules
#include "owCore/openworld_core.h"
#include "owWindow/openworld_window.h"
// Header for testing
#include "owCore/test.h"
//
using namespace OpenWorld::Core;

#include <sol.hpp>

// Instance
#include "owCore/Types/instance.h"
#include "owWindow/Types/part.h"
using namespace OpenWorld::Types;

// Test C++ Interface of heirachy
TEST(C_Instance)
{
    Instance par("parent");
    Instance child("child", &par);

    CHECK_EQUAL(&par, child.getParent());
    CHECK_EQUAL(par.findFirstChild("child", false), &child);

    Instance test("Test");
    child.setParent(&test);

    CHECK_EQUAL(&test, child.getParent());
}

// Test if Lua interface for Instance
TEST(Lua_Instance)
{
    OpenWorld::Core::TestRunner<> s;
    s.eval("p = Instance.new(\"Parent\")");
    s.eval("c = Instance.new(\"Child\", p)");  // setParent CTOR
    s.eval("r = p:findFirstChild(\"Child\",false)");

    CHECK_EQUAL(s.eval<bool>("r == c"), true);
    CHECK_EQUAL(s.eval<bool>("p == c"), false);
    CHECK_EQUAL(s.eval<bool>("p == c.Parent"), true);
    CHECK_EQUAL(s.eval<bool>("c == p.Child"), true);

    s.eval("n = Instance.new(\"T\")");
    s.eval("c.Parent = n");

    CHECK_EQUAL(s.eval<bool>("c.Parent == n"), true);
}

// Events
TEST(Lua_Event)
{
    TestRunner<> s;

    s.eval("ca_fired = false");
    s.eval("cr_fired = false");
    s.eval("pc_fired = false");

    s.eval("p = Instance.new(\"Parent\")");
    s.eval("p.ChildAdded:connect(function() ca_fired=true; end)");
    s.eval("c = Instance.new(\"Child\", p)");  // setParent CTOR

    // ca_fired set to true

    s.eval("p.ChildRemoved:connect(function() cr_fired=true; end)");
    s.eval("c.ParentChanged:connect(function() pc_fired=true; end)");

    // cr_fied && pc_fired set to true

    s.eval("n = Instance.new(\"Test\")");
    s.eval("c.Parent = n");

    CHECK_EQUAL(s.eval<bool>("ca_fired"), true);
    CHECK_EQUAL(s.eval<bool>("cr_fired"), true);
    CHECK_EQUAL(s.eval<bool>("pc_fired"), true);
}

#include "owWindow/Types/window.h"
// GUI primitives
TEST(GUI_Primitives)
{
    TestRunner<OpenWorld::Types::Window> s;

    s.eval("t = UDim.new(0.5,0)");
    s.eval("f = UDim.new(0.5,0)");
    s.eval("vec = UVector2.new(t,f)");

    CHECK_EQUAL(s.eval<CEGUI::UDim>("t+f"), CEGUI::UDim(1.0, 0));
}

TEST(Inheritance_Macros)
{
    TestRunner<OpenWorld::Types::Part> s;
    s.eval("p = Part.new(\"Part\", root)");
    s.eval("i = Instance.new(\"Instance\", p)");

    CHECK_EQUAL(s.eval<std::string>("p.Name"), "Part");
    CHECK_EQUAL(s.eval<int>("#p:getChildren()"), 1);
}